# Code Interview for DireGaming

## Stack
The app is built using Laravel and Vue.js. The Vue.js scaffolding is generated using the laravel/ui package.

There are three main 'vue' files, App, Tile and Search. The Laravel package only serve as a backend that serve the Vue.js front end UI.

The development of this app is done using Vagrant with Homestead configuration.

## Install
First, install the relevant PHP packages for the Lavavel environment.

`composer install`

Then, install the relevant JavaScript packages for the Vue.js environment.

`npm install`

To start the app:

`npm run dev`